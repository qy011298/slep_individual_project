# SLEP Individual Coursework

## Preamble

Module Code: CS3SC17

Assignment Report Titles: Paper and Reflections

Student Number: 26011298

Date (when completed:)

Actual Hrs spent for Assignemnt:

Assignment Evaluation:

## Abstract

## Introduction

Social, legal and ethical aspects are often taken into consideration when media outlets report on stories about technology. The purpose of this report is to discuss the potential implications to these aspects, that were highlighted in a few articles about a recent media story, as well as discuss what implications may be missing from them. The story this report will be looking at is the proposal to ban communication/social media services that make use of end-to-end encryption. This media story was spun after a terrorist attack had caused the (then) PM David Cameron to make a speech regarding the technologies that terrorists often use to communicate. A video for this speech can be found in the Guardian article [2].

## Analysis

For the analysis of this media story, several articles will be considered, including from the BBC [1], The Guardian [2] and The NextWeb [3]. Certain points will also be considered from an online video by Tom Scott [4].

The vast majority of points raised by the sources are specifically against the banning of encrypted end-to-end messages, however some points are highlighted that are in favour. For instance, an argument for the ban is that this will allow agencies, tasked to protect the public from terror attacks, to access communications, thereby closing "safe spaces" used by suspected terrorists. This would result in a reduced risk of terrorist attacks and an overall increase in safety to the general public. It is also mentioned in Tom Scott's video [4] that a similar system has been in place for years, but with telephones and physical telephone lines. This is known as wiretapping and is legally allowed under the Regulation of Investigatory Powers Act [5], when in the interest of national security and public safety. It could be considered that the speech made by David Cameron was merely suggesting an equivalent for internet based messaging services, however the articles speculated otherwise.

Despite this potential increase in safety to the general public, several points were raised in these articles against the proposed legislation. A fairly major point identified by all sources is that the encryption of end-to-end messages are used for services such as banking and e-commerce, that benefit the general public. As The Guardian put it: "There is no such thing as “good guy encryption” and “bad guy encryption”.". The belief of the articles here is that removing encryption in services that allow terrorists to communicate, does not justify the potential security risks and damage to beneficial services that a much larger proportion of the public use. Had legislation been put in, very careful wording would have had to be used to specify the ban extends only to social media that terrorists are known to use, not services that underpin a signficiant portion of business and economic infrastructure.

Many of the articles make reference to the rhetorical question David Cameron asked in his speech being; "[I]n our country, do we want to allow a means of communication between people which we cannot read?", with all articles responding with "yes". The argument presented against this, is that without a means of communication that the government can't read, the freedom of expression (granted in the Humans Rights Act 1998 [6], Article 10) and the right to privacy (Human Rights Act 1998 [6], Article 8) of the general public is greatly diminished. A counter argument often used in situations relating to surveillance of the general public (such as CCTV) is the idea of "Nothing to hide, nothing to fear"; in that if someone is not breaking the law, they wouldn't be affected by the surveillance. In addition, the aforementioned rights of the public have clauses that allow infringement, when in the interest of national safety; in which, a case could be made that unencrypted messages are soley in the interest of national safety.

A significant legal issue indentified in the sources is the inability for UK law and legislation to force software and apps built in other countries to either stop using encryption or to implement a backdoor. If legislation was passed successfully, to ban encryption within software without a backdoor for the UK government, companies would have 2 options: remove all encryption from their services or, the more likely option, stop providing the service in the UK.

If the company decided on the former option, this poses a signficant security threat to both the UK, as well as other countries that the software operates in. The prior argument mentioned in favour of government surveillance of "Nothing to hide, nothing to fear" becomes less of a matter of morality, and becomes a matter of, do people share the same values as the government of the country they're in. For example, being homosexual in the UK is not against any law or is considered abhorrent, however, this is against the law in some middle easten countries, and so people using a messaging service that their government has easy access to could pose a threat to them. Therefore, the UK's proposed legislation could affect the lives of citizens of other countries, not just the UK's.

The latter option, of companies refusing to provide their services in the UK, is hardly much better. Within the last few decades, with the development of technology and the Internet, the general public has become accustom, and dependent on a variety of technology provided by companies outside of the UK. Examples of this include Facebook's WhatsApp messaging service, or Apple's IPhone, whose given by the BBC and TheNextWeb as an example of when a company has, in the past, refused efforts from the government to change their technology. As it's not formally mentioned how comprehensive this proposed legislation is, in regards to which services this will affect, the potential here is that so many companies would refuse to change their technology and subsequently stop offering their services, that the UK becomes isolated and more analogue as a result.

The other option presented by many of these articles is the idea of an encryption backdoor, allowing services to maintain their encryption but allow the UK government to break that encryption, easily. This idea has been slated by TheNextWeb and BBC, for the additional security issues that could occur from this, with the BBC citing an MD of an internet hosting firm: "A UK cryptographic backdoor would become the number one global target for cybercriminals.". The main concern here is the belief that a backdoor accessible to the UK government wouldn't be possible to implement, without potential security risks. While this would be the case in practice, the theory of backdoor implementation is a reasonable one, as it would allow the UK government to more easily keep track of people of interest, while maintaining the general public's right to privacy. If the implementaion of a backdoor could somehow be guarenteed secure, many arguments against the government surveillance of end-to-end encrypted messages would be invalid, on the condition that the power to read these messages are actually limited to what's stated under RIPA. [5]

Since the articles were written in 2015, there has since been legislation written into law that this proposed banning of end-to-end encryption would be directly against, the General Data Protection Regulation (GDPR) [7]. While encryption is not explicity required as part of GDPR [8], when handelling personal data, there is a requirement that the controller of the data must "implement measures designed to (...) prevent unauthorised processing" (Part 3, Chapter 4, Section 66) [7]. While abolishment of encryption doesn't necessarily mean there aren't any other methods implemented, its the most widely accepted and used method of protecting data. In addition, few other methods can be used to secure data when in transit (such as messages being sent), and so the companies would become very limited in how they could abide to GDPR.

## Conclusion

## References

1. J.Wakefield (2015) _Can the Government ban encryption?_ Available at: https://www.bbc.co.uk/news/technology-30794953 (Accessed: Nov 6th 2020)

2. J.Ball (2015) _Cameron wants to ban encryption - he can say goodbye to digital Britain_ Available at: https://www.theguardian.com/commentisfree/2015/jan/13/cameron-ban-encryption-digital-britain-online-shopping-banking-messaging-terror (Accessed: Nov 6th 2020)

3. O.Williams (2015) _David Cameron's plan to ban end-to-end encryption is catastrophic for internect freedom_ Available at: https://thenextweb.com/opinion/2015/01/13/david-camerons-plan-ban-end-end-encryption-catastrophic-internet-freedom/ (Accessed: Nov 6th 2020)

4. T.Scott (2017) _Why the Government Shouldn't break WhatsApp_ Available at: https://www.youtube.com/watch?v=CINVwWHlzTY (Accessed: Nov 6th 2020)

5. _Regulation of Investigatory Powers Act 2000_ (2000) Section 28. Available at: https://www.legislation.gov.uk/ukpga/2000/23 (Accessed: Nov 16th 2020)

6. _Human Rights Act 1998_ (1998). Available at: https://www.legislation.gov.uk/ukpga/1998/42/schedule/1/part/I/chapter/9 (Accessed: Nov 16th 2020)

7. _Data Protection Act 2018_ (2018) Available at: https://www.legislation.gov.uk/ukpga/2018/12/contents/enacted (Accessed: 30th Nov)

8. _Art32. GDPR- Security of Processing_ Available at: https://gdpr-info.eu/art-32-gdpr/ (Accessed: 30th Nov 2020)

## Reflection on CS3CS17

